
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andre
 */
public class Pratica31 {
    private static Date inicio;
    private static String meuNome;
    private static GregorianCalendar dataNascimento;
    private static Date fim;
    public static void main(String[] args) {
        inicio = new Date();
        meuNome = "André Luis Ribeiro";
        System.out.println(meuNome.toUpperCase());
        String[] nomes = meuNome.split(" ");
        String sobrenome = nomes[nomes.length-1].substring(0,1).toUpperCase() + nomes[nomes.length-1].substring(1).toLowerCase();
        String sigla = ", ";
        for(int i = 0; i < nomes.length-1; i++) {
            sigla += nomes[i].charAt(0) + ". ";
        }
        System.out.println(sobrenome + sigla.toUpperCase().trim());
        
        dataNascimento  = new GregorianCalendar(1997, 9, 15);
        fim = new Date();
        System.out.println((fim.getTime() - dataNascimento.getTime().getTime())/(1000*60*60*24));
        System.out.println(fim.getTime() - inicio.getTime());
        
        
    }
}
